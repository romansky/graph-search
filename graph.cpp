#include "graph.h"
#include <iostream>

using namespace std;

/*
   enum color_t
   {
   black, grey, white
   };

   struct node
   {
   color_t color;
   int distance;
   int parent;
   };
   */
void graph::addVertex(int n)
{
    auto search = adj.find(n);
    if(search == adj.end()) {
        adj[n] = vector<int>();
        vertices.push_back(n);
    }
}

void graph::addEdge(int f, int l)
{
    addVertex(f);
    addVertex(l);
    if(f != l) {
        adj[f].push_back(l);
    }
    adj[l].push_back(f);
}

void graph::printAdj()
{
    for(auto i = adj.begin(); i != adj.end(); i++)
    {
        cout << "Node: " << i->first << endl;
        for(int i : i->second)
        {
            cout << i << " ";
        }
        cout << endl;
    }
}

void graph::bfs(int v)
{
    cout << "Starting Node: " << v << endl;
    bool find = false;
    map<int, node> cursor;

    for(auto i = vertices.begin(); i != vertices.end(); i++)
    {
        if(*i == v) {
            find = true;
        }
        cursor[*i] = node{white, numeric_limits<int>::max(), -1};
    }
    cursor[v].color = grey;
    cursor[v].distance = 0;
    queue<int> q;
    q.push(v);
    while(false == q.empty()) // Yoda coding
    {
        int front = q.front();
        q.pop();
        for(auto i = adj[front].begin(); i != adj[front].end(); i++)
        {
            if(cursor[*i].color == white) {
                cursor[*i].color = grey;
                cursor[*i].distance = cursor[front].distance + 1;
                cursor[*i].parent = front;
                q.push(*i);
                cout << *i << " ";
            }
        }
        cursor[front].color = black;
    }
    cout << endl;
}

void graph::dfs()
{
    while(!endNodes.empty())
    {
        endNodes.pop();
    }
    map<int, node> cursor;
    for(auto i = vertices.begin(); i != vertices.end(); i++)
    {
        cursor[*i] = node{white, numeric_limits<int>::max(), -1};
    }
    time = 0;
    for(auto i = vertices.begin(); i != vertices.end(); i++)
    {
        if(cursor[*i].color == white) {
            dfsVisit(cursor, *i);
        } else if (cursor[*i].color == grey) {
            acyclic = true;
        }
    }
    cout << endl;
}

void graph::dfsVisit(map<int,node>& cursor, int v)
{
    cout << v << " ";
    time++;
    cursor[v].distance = time;
    cursor[v].color = grey;
    for(auto i = adj[v].begin(); i != adj[v].end(); i++)
    {
        if(cursor[*i].color == white) {
            cursor[*i].parent = v;
            dfsVisit(cursor, *i);
        } else if (cursor[*i].color == grey) {
            acyclic = true;
        }
    }
    cursor[v].color = black;
    time++;
    cursor[v].finish = time;
    endNodes.push(v);
}

bool graph::isDirected()
{
    return directed;
}

void graph::flipEdge(int v1, int v2)
{
    addVertex(v1);
    addVertex(v2);
    if(!directed){
        revAdj[v1].push_back(v2);
    } else {
        revAdj[v2].push_back(v1);
    }
}

void graph::transpose()
{
    for(auto i = revAdj.begin(); i != revAdj.end(); i++)
    {
        int v1 = i->first;
        for(int n : i->second)
        {
            int v2 = n;
            flipEdge(v1,v2);
        }
    }
}

bool graph::dag()
{
    if(directed && !acyclic) {
        return true;
    } else {
        return false;
    }
}

void graph::topoSort()
{
    if(dag()){
        stack<int> tmp = endNodes;
        while(!tmp.empty())
        {
            cout << tmp.top() << " ";
            tmp.pop();
        }
    } else {
        cout << "Not a DAG" << endl << endl;
    }
    cout << endl;
}

void graph::component()
{
    transpose();
    componentDfs();
}

void graph::componentDfs()
{
    map<int, node> cursor;
    for(auto i = vertices.begin(); i != vertices.end(); i++)
    {
        cursor[*i] = node{white, numeric_limits<int>::max(), -1};
    }
    time = 0;
    while(!endNodes.empty())
    {
        if (cursor[endNodes.top()].color == white) {
            int top = endNodes.top();
            cDfsVisit(cursor, top);
        } else if (cursor[endNodes.top()].color == grey) {
            acyclic = true;
        }

        endNodes.pop();
    }
    cout << endl;
}

void graph::cDfsVisit(map<int, node>& cursor, int u)
{
    cout << u << " ";
    time++;
    cursor[u].distance = time;
    cursor[u].color = grey;
    for(auto i = adj[u].begin(); i != adj[u].end(); i++)
    {
        if (cursor[*i].color == white) {
            cursor[*i].parent = u;
            dfsVisit(cursor, u);
        } else if (cursor[*i].color == grey) {
            acyclic = true;
        }
    }
    cursor[u].color = black;
    time++;
    cursor[u].finish = time;
    cout << endl;
}

void graph::vertexCover(map<int, node>& cursor, int u)
{
    bool visited[vertices.size()];
    for(int i=0; i < vertices.size(); i++)
    {
        visited[i] = false;
    }
    for (auto i = adj[u].begin(); i != adj[u].end(); i++)
    {
        if(visited[u] == false) {
            for(auto i = adj[u].begin(); i != adj[u].end(); i++)
            {
                int v = *i;
                if(visited[u] == false) {
                    visited[u] = true;
                }

            }
        }
    }
}

