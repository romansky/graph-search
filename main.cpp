#include "graph.h"
#include <iostream>

using namespace std;

int main()
{
    graph g;
    int choice; // User input
    int v1, v2; // Vertices to be connected
    int subV; // Starting vertex for the searching algorithms

    do
    {
        cout << "1 - Add an edge" << endl;
        cout << "2 - Print the Adjacency list of the graph" << endl;
        cout << "3 - Print using BFS" << endl;
        cout << "4 - Print using DFS" << endl;
        cout << "5 - Perform a topological sort on the graph" << endl;
        cout << "6 - Print the component graph" << endl;
        cout << "7 - Exit the program" << endl;
        cout << "Input: ";
        cin >> choice;

        switch(choice)
        {
            case 1:
                cout << "Vertex 1: ";
                cin >> v1;
                cout << "Vertex 2: ";
                cin >> v2;
                g.addEdge(v1,v2);
                break;
            case 2:
                g.printAdj();
                break;
            case 3:
                cout << "Enter the starting vertex: ";
                cin >> subV;
                g.bfs(subV);
                break;
            case 4:
                g.dfs();
                break;
            case 5:
                g.topoSort();
                break;
            case 6:
                g.component();
                break;
            case 7:
                return 0;
        }

    } while (true);
}
