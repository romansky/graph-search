#ifndef GRAPH_H
#define GRAPH_H
#include <vector>
#include <queue>
#include <map>
#include <list>
#include <stack>

using namespace std;

enum color_t
{
    black, grey, white

};

struct node
{
    color_t color;
    int distance;
    int parent;
    int finish;
};

class graph
{
    private:
        map<int, vector<int>> adj;
        map<int, vector<int>> revAdj;
        list<int> vertices;
        bool directed;
        bool acyclic = false;
        stack<int> endNodes;
        int time;
    public:
        void addVertex(int n);
        void addEdge(int f, int l);
        void printAdj();
        void bfs(int n);
        void dfs();
        void dfsVisit(map<int,node>& cursor, int u);
        bool isDirected();
        void flipEdge(int, int);
        void transpose();
        bool dag();
        void topoSort();
        void component();
        void componentDfs();
        void cDfsVisit(map<int, node>& cursor, int u);
        void vertexCover(map<int, node>& cursor, int u);
};

#endif
