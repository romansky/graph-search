# Graph Searching Algorithms
**Billy Romansky**

This repository implements breadth and depth first search algorithms on simple connected graphs.

Design:
- Uses the map standard libarary to hold an int as a label of a vertex and a vector to keep track of adjacent vertices. The list standard library was used to track the attributes of each node.

Analysis:
- **Breadth First Search**: The best case complexity of BFS for a simple graph with vertices $`v`$ and edges $`v`$ is $`O(|v|)`$ while the worst case is $`O(|v|+|e|)`$

- **Depth First Search**: The best case complexity of DFS for a simple graph with vertices $`v`$ and edges $`v`$ is $`O(|v|)`$ while the worst case is $`O(|v|+|e|)`$

## TODO
- [x] Implement Breadth First Search
- [x] Implement Depth First Search
